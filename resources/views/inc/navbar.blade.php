<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/">{{config("app.name",'project2')}}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/about">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="/services">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="/posts">Blog</a>
            </li>
        </ul>
        <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>











{{--<nav class="navbar navbar-inverse">--}}
{{--    <div class="container">--}}
{{--        <div class="navbar-header">--}}
{{--            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">--}}
{{--                <span class="sr-only">Toggle Navigation</span>--}}
{{--                <span class="icon-bar"></span>--}}
{{--                <span class="icon-bar"></span>--}}
{{--                <span class="icon-bar"></span>--}}
{{--            </button>--}}
{{--            <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'project2') }}</a>--}}
{{--        </div>--}}
{{--        <div id="navbar" class="collapse navbar-collapse">--}}
{{--            <ul class="nav navbar-nav">--}}
{{--                <li class="active"><a href="/">Home</a></li>--}}
{{--                <li><a href="/about">About</a></li>--}}
{{--                <li><a href="/services">Services</a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</nav>--}}






































{{--<nav class="navbar navbar-inverse">--}}
{{--    <div class="container">--}}
{{--        <div class="navbar-header">--}}
{{--            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse">--}}
{{--            <span class="sr-only">Toggle navigation</span>--}}
{{--            <span class="icon-bar"></span>--}}
{{--            <span class="icon-bar"></span>--}}
{{--            <span class="icon-bar"></span>--}}
{{--            </button>--}}
{{--            <a class="navbar-brand" href="/">{{config('app.name', 'LSAPP')}}</a>--}}
{{--        </div>--}}
{{--        <div id="navbar" class="collapse b=navbar-collapse">--}}
{{--            <ul class="nav navbar-nav">--}}
{{--                <li><a href="/">Home</a></li>--}}
{{--                <li><a href="/about">About</a></li>--}}
{{--                <li><a href="/services">Services</a></li>--}}
{{--                <li><a href="/posts">Blog</a></li>--}}
{{--            </ul>--}}
{{--            <ul class="nav navbar-nav navbar-right">--}}
{{--                <li><a href="/posts/create">Create Post</a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</nav>--}}



{{--1. In local project2 folder type:--}}
{{--$ composer require unisharp/laravel-ckeditor--}}

{{--2. And then add this line to config/app.php:--}}
{{--Unisharp\Ckeditor\ServiceProvider::class,--}}

{{--3. Publish the resources: (In local project2 folder)--}}
{{--$php artisan vendor:publish --tag=ckeditor--}}
